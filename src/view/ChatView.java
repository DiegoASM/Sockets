package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import sockets.TCPClient;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChatView extends JFrame {

	private JPanel contentPane;
	private JTextField textip;
	private JTextField textpuerto;
	private JTextField mensajeText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChatView frame = new ChatView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChatView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 460);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblIp = new JLabel("IP");
		lblIp.setBounds(42, 37, 46, 14);
		contentPane.add(lblIp);

		JLabel lblPuerto = new JLabel("Puerto");
		lblPuerto.setBounds(42, 78, 46, 14);
		contentPane.add(lblPuerto);

		JTextArea historialTa = new JTextArea();
		historialTa.setBounds(42, 118, 366, 194);
		contentPane.add(historialTa);

		textip = new JTextField();
		textip.setBounds(98, 34, 202, 20);
		contentPane.add(textip);
		textip.setColumns(10);

		textpuerto = new JTextField();
		textpuerto.setBounds(98, 75, 202, 20);
		contentPane.add(textpuerto);
		textpuerto.setColumns(10);

		mensajeText = new JTextField();
		mensajeText.setBounds(42, 348, 238, 20);
		contentPane.add(mensajeText);
		mensajeText.setColumns(10);

		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String ip = textip.getText();
				int puerto = Integer.parseInt(textpuerto.getText());
				String mensaje = mensajeText.getText();
				String mensajeDeServidor = TCPClient.enviarMensaje(ip, puerto, mensaje);
				historialTa.append(mensajeDeServidor + "\n");
			}
		});
		btnEnviar.setBounds(308, 347, 89, 23);
		contentPane.add(btnEnviar);
	}
}
