package DAOH;
import entidad.historial;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import conexionbasededatos.MysqlConnect;

public class historialDAO implements IhistorialDAO{

MysqlConnect con;
	
	public historialDAO(){
		con = new MysqlConnect();
		
		con.connect();
	}

	@Override
	public void agregar(historial historial) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("INSERT INTO historial"
																		+ "(puerto, ip, mensaje)" 
																		+ "VALUES(?,?,?)");
			statement.setString(1, historial.getPuerto());
			statement.setInt(2, historial.getIp());
			statement.setString(3, historial.getMensaje());		
			statement.executeQuery();		
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}

